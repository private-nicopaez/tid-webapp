require_relative '../spec_helper.rb'

RSpec.describe 'Dashboard' do

    describe 'wip_status' do

      it 'should be ok when below 8' do
        dashboard = Dashboard.new
        dashboard.wip=5
        expect(dashboard.wip_status).to eq 'panel-green'
      end

      it 'should be soso when between 8 and 10' do
        dashboard = Dashboard.new
        dashboard.wip=9
        expect(dashboard.wip_status).to eq 'panel-yellow'
      end

      it 'should be bad when over 10' do
        dashboard = Dashboard.new
        dashboard.wip=11
        expect(dashboard.wip_status).to eq 'panel-red'
      end

    end

    describe 'pull_status' do

      it 'should be green when below 4' do
        dashboard = Dashboard.new
        dashboard.pull=3
        expect(dashboard.pull_status).to eq 'panel-green'
      end

      it 'should be red when over 3' do
        dashboard = Dashboard.new
        dashboard.pull=9
        expect(dashboard.pull_status).to eq 'panel-red'
      end

    end

  describe 'days_without_incidents' do

    it 'should return unknow when no data available' do
      ENV[Dashboard::LAST_INCIDENT_DATE]=nil
      dashboard = Dashboard.new
      expect(dashboard.days_without_incidents).to eq('?')
    end

    it 'should return the days has passed since then' do
      count = 5
      ENV[Dashboard::LAST_INCIDENT_DATE] = (Date.today-count).to_s
      dashboard = Dashboard.new
      expect(dashboard.days_without_incidents).to eq(count)
    end

  end
end