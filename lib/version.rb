class Version

  MAJOR = 0
  MINOR = 1
  BUILD = 1

  def self.current
    "#{MAJOR}.#{MINOR}.#{BUILD}"
  end

end