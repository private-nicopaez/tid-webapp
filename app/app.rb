module Tid
  class App < Padrino::Application
    enable :sessions
    register Padrino::Rendering
    register Padrino::Helpers
    
    get '/' do
      dashboard = Dashboard.new
      @pull_requests_count = dashboard.pull_request_count
      @pull_status = dashboard.pull_status
      @delivered_stories = dashboard.delivered_stories
      @wip = dashboard.wip
      @wip_status = dashboard.wip_status
      @days_without_incidents = dashboard.days_without_incidents
      render :index
    end

    get '/env' do
      return ENV[Dashboard::GITHUB_LOGIN]
    end

  end
end
