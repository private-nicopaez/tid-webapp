require 'trello'

class Dashboard

  LAST_INCIDENT_DATE = 'LAST_INCIDENT_DATE'
  TRELLO_DEVELOPER_KEY = 'TRELLO_DEVELOPER_KEY'
  TRELLO_MEMBER_TOKEN = 'TRELLO_MEMBER_TOKEN'
  TRELLO_DONE_LIST= 'TRELLO_DONE_LIST'
  TRELLO_WIP_LIST = 'TRELLO_WIP_LIST'
  TRELLO_DASHBOARD_ID = 'TRELLO_DASHBOARD_ID'
  GITHUB_LOGIN = 'GITHUB_LOGIN'
  GITHUB_PASSWORD = 'GITHUB_PASSWORD'
  GITHUB_ORG_NAME = 'GITHUB_ORG_NAME'

  def initialize
    @pull_requests_count = 0
    @wip = 0
    @client = Octokit::Client.new(:login => ENV[Dashboard::GITHUB_LOGIN], :password => ENV[Dashboard::GITHUB_PASSWORD])
  end

  def delivered_stories
    Trello.configure do |config|
      config.developer_public_key = ENV[Dashboard::TRELLO_DEVELOPER_KEY]
      config.member_token = ENV[Dashboard::TRELLO_MEMBER_TOKEN]
    end
    @stories = Trello::List.find(ENV[Dashboard::TRELLO_DONE_LIST]).cards.size
    return @stories
  end

  def pull_request_count
    repos = @client.get("/orgs/#{ENV[Dashboard::GITHUB_ORG_NAME]}/repos")
    @pull_requests_count = 0
    repos.each do | repo |
      @pull_requests_count += @client.get("/repos/#{ENV[Dashboard::GITHUB_ORG_NAME]}/#{repo.name}/pulls").size;
    end
    return @pull_requests_count
  end

  def wip
    Trello.configure do |config|
      config.developer_public_key = ENV[Dashboard::TRELLO_DEVELOPER_KEY]
      config.member_token = ENV[Dashboard::TRELLO_MEMBER_TOKEN]
    end
    @wip = Trello::List.find(ENV[Dashboard::TRELLO_WIP_LIST]).cards.size
    return @wip
  end

  def wip=(val)
    @wip = val
  end

  def pull=(val)
    @pull_requests_count = val
  end

  def pull_status
    return 'panel-green' if @pull_requests_count <=3
    return 'panel-red'
  end

  def wip_status
    return 'panel-green' if @wip <=8
    return 'panel-yellow' if @wip <=10
    return 'panel-red'
  end

  def days_without_incidents
    last_incident_date = ENV[Dashboard::LAST_INCIDENT_DATE]
    if last_incident_date.nil?
      return '?'
    end
    since = Date.parse(last_incident_date)
    return (Date.today - since).to_i
  end

end